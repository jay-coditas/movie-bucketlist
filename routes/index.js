import express from 'express';
import MovieController from '../moviesControllers/movies';

const router = express.Router();

router.get('/api/v1/movies',MovieController.getAllMovies);

router.post('/api/v1/movies',MovieController.createMovie);

router.get('/api/v1/movies/:id',MovieController.getMovie);

router.delete('/api/v1/movies/:id',MovieController.deleteMovie);

router.put('/api/v1/movies/:id',MovieController.updateMovie);

export default router;