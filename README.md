# Movie bucketlist

Created a set of restful APIs using node | express | sequelize | postgresql

### To seed the database
```npm run seed``` (via json)

```npm run seed-csv``` (via csv)

### To start the server
```npm run start```
