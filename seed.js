import {movies, db} from './models/index.js';
import * as data from './config/seeds/data.json';

const {content} = data;

const seed = async () => {
    await db.sync({force: true});

    for(const movie of content){
        await movies.create(movie);
    }
    
    db.close();
    console.log("Seed successful !");
}

seed();