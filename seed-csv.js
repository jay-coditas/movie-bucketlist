import csv from 'csv-parser'
import fs from 'fs';
import {movies, db} from './models/index.js';

// Populates database by reading the entire file
// Extracts individual records from csv to populate it in the database

(async ()=>{
    await db.sync({force: true});
    fs.createReadStream('./config/seeds/data.csv')
    .pipe(csv())
    .on('data', async (row)=>{
        await movies.create(row);
    })
    .on('end', ()=>{
        db.close();
        // Reading of file completed
    })
})();
