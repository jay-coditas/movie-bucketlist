import {movies} from '../models/index';

class MoviesController {
    async getAllMovies(request,response){
        try {
            const allMovies = await movies.findAll({
                order:[
                    ['createdAt']
                ]
            });
            response.status(200).send({
                success: 'true',
                message: 'Movies retrieved successfully',
                allMovies
            });
        } catch(error) {
            response.status(404).send({
                success: 'false',
                message: 'Unable to retrieve movies'
            });
        }
    }

    async getMovie(request,response){
        const id = parseInt(request.params.id,10);
        await movies.findOne({
            where: {
                id: id
            }
        })
        .then((movie)=>{
            if(!movie) throw error;
            return response.status(200).send({
                success: 'true',
                message: 'Movie retrieved successfully',
                movie
            });
        })
        .catch((err)=>{
            return response.status(404).send({
                success: 'false',
                message: 'Unable to retrieve specified movie',
                err
            });
        });
    }

    async createMovie(request,response){
        const result = await movies.findAndCountAll();
        const movie = {
            id: result.count + 1,
            title: request.body.title,
            description: request.body.description,
            name: request.body.name,
            genre: request.body.genre,
            seen: request.body.seen
        };
        
        await movies.create(movie)
        .then(()=>{
            return response.status(201).send({
                success: 'true',
                message: 'Movie created successfully',
                movies
            });
        })
        .catch((err)=>{
            return response.status(404).send({
                success: 'true',
                message: 'Unable to create movie',
                err
            });
        })
        
    }

    async deleteMovie(request,response){
        const id = parseInt(request.params.id,10);
        await movies.destroy({
            where: {
                id: id
            },
            force: true
        })
        .then((numbers)=>{
            if(!numbers) throw error 
            return response.status(200).send({
                success: 'true',
                message: 'Movie deleted successfully',
                movies
            });
        })
        .catch((error)=>{
            return response.status(404).send({
                success: 'false',
                message: 'Specified movie does not exist ',
                error
            });
        });

    }

    async updateMovie(request,response){
        const id = parseInt(request.params.id,10);
    
        await movies.update({
            title: request.body['title'],
            description: request.body['description'],
            name: request.body['name'],
            genre: request.body['genre'],
            seen: request.body['seen']
        },
        {
            where: {
                id: id
            }
        })
        .then((res)=>{
            if(!res[0]) throw error
            return response.status(200).send({
                success: 'true',
                message: 'Movie updated successfully',
            });
        })
        .catch((error)=>{
            return response.status(404).send({
                success: 'false',
                message: 'Movie not found',
                error
            });
        });
    }
    
}

const MovieController = new MoviesController();

export default MovieController;